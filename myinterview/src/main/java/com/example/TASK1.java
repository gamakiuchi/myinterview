package com.example;
import java.util.Scanner;
/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Digite uma palavra :");
        String palavra = scan.nextLine().toLowerCase();

        String palavraInvertida = new StringBuffer(palavra).reverse().toString();

        if(palavra.equals(palavraInvertida))
            System.out.println("É Palindrome!");
        else
            System.out.println("Não é Palindrome!");
    }
 
}